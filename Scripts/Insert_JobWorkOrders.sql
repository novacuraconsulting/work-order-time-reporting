BULK INSERT JobWorkOrders
FROM 'C:\Mistras\JobOrders.csv'
WITH
(
FIRSTROW = 2,
FIELDTERMINATOR = ',',
ROWTERMINATOR = '\n'
)
