/****** Object:  Table [dbo].[SwipeReport]    Script Date: 1/19/2018 11:25:59 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SwipeReport](
	[ID] [varchar](40) NULL,
	[FullName] [varchar](40) NULL,
	[SwipeIn] [varchar](100) NULL,
	[SwipeOut] [varchar](100) NULL,
	[HoursWorked] [varchar](10) NULL
) ON [PRIMARY]
GO


