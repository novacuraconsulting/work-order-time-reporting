/****** Object:  Table [dbo].[EmployeeTimeReport]    Script Date: 1/3/2018 3:43:16 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EmployeeTimeReport](
	[ID] [uniqueidentifier] DEFAULT NEWSEQUENTIALID() NOT NULL,	
	[EmployeeID] [int] NOT NULL,
	[IFS Employee ID] [int] NULL,
	[RAC Job Id] [nvarchar](50) NOT NULL,
	[PO Number] [nvarchar](50) NOT NULL,
	[WO Number] [nvarchar](50) NOT NULL,
	[RT] [int] NULL,
	[OT] [int] NULL,
	[DT] [int] NULL,
	[ReportDate] [datetime] NOT NULL,
 CONSTRAINT [PK_EmployeeTimeReport] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


