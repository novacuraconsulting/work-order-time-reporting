BULK INSERT Employees
FROM 'C:\Projects\Mistras\Employees.csv'
WITH
(
FIRSTROW = 2,
FIELDTERMINATOR = ',',
ROWTERMINATOR = '\n'
)
