USE [MISTRAS]
GO

/****** Object:  Table [dbo].[Employees]    Script Date: 12/1/2017 3:49:48 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Employees](
	[ID] [int] NOT NULL,
	[IFS Employee ID] [int] NULL,
	[LastName] [nvarchar](50) NULL,
	[FirstName] [nvarchar](50) NULL,
	[FullName] [nvarchar](100) NULL,
	[SkillCode] [nvarchar](20) NULL,
	[Company] [nvarchar](20) NULL,
	[Shift] [nvarchar](10) NULL,
	[Active] [char](1) NULL,
	[Phone] [nvarchar](25) NULL,
	[Email] [nvarchar](320) NULL,
	[Covies] [nvarchar](10) NULL,
	[Sales Part Number] [nvarchar](50) NULL,	
 CONSTRAINT [PK_Employees] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


