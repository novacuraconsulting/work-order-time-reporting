USE [MISTRAS]
GO

/****** Object:  Table [dbo].[JobWorkOrders]    Script Date: 12/1/2017 3:56:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[JobWorkOrders](
	[RAC Job Id] [nvarchar](50) NOT NULL,
	[Job] [nvarchar](500) NULL,
	[PO Number] [nvarchar](50) NULL,
	[WO Number] [nvarchar](50) NULL,
	[Approver] [nvarchar](100) NULL,
	[Active] [nvarchar](10) NULL,
	[Status] [nvarchar](50) NULL,
 CONSTRAINT [PK_JobWorkOrders] PRIMARY KEY CLUSTERED 
(
	[RAC Job Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


